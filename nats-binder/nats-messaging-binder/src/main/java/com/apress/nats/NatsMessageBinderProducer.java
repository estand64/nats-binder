package com.apress.nats;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.provisioning.ConsumerDestination;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.messaging.Message;
import org.springframework.util.SerializationUtils;

@Slf4j
public class NatsMessageBinderProducer extends MessageProducerSupport {

//    private ConsumerDestination destination;
    private NatsMessageListenerAdapter adapter;

    public NatsMessageBinderProducer(ConsumerDestination destination, JetStreamConnection jetStreamConnection){
        if(destination == null){
            log.error("Destination can't be null when attempting to create a " + getClass().getName());
            return;
        }
        if(jetStreamConnection == null){
            log.error("JetStream connection can't be null when attempting to create a " + getClass().getName());
            return;
        }
        NatsMessageListenerAdapter.builder()
                .subject(destination.getName())
                .jetStreamConnection(jetStreamConnection)
                .adapter(messageListener)
                .build();
    }

    @Override
    protected void doStart() {
        adapter.start();
    }

    @Override
    protected void doStop() {
        adapter.stop();
        super.doStop();
    }

    private NatsMessageListener messageListener = message -> {
        log.debug("[BINDER] Message received from NATS: {}", message);
        log.debug("[BINDER] Message Type received from NATS: {}", message.getClass().getName());
        sendMessage((Message<?>)SerializationUtils.deserialize(message));
    };
}
