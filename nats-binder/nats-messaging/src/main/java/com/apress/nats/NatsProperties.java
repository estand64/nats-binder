package com.apress.nats;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collection;

@Data
@ConfigurationProperties("nats")
public class NatsProperties {
    private String host = "localhost";
    private int port = 4222;
    private Collection<String> streams;
}
